﻿using PostaBiztosito.Exception;
using System;

namespace PostaBiztosito
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Kérem a CDW stringet:");
            var cInputString = Console.ReadLine();
            
            Console.Write("Kérem a hosszúságot:");
            var cLength = Console.ReadLine();

            try
            {
                if (Int32.TryParse(cLength, out int nLength))
                {
                    CDVNumber.IsPostaBiztositoCDV(cInputString, nLength);
                }
                else
                {
                    Console.Write("A hosszúság csak számjegyet tartalmazhat.");
                    Console.ReadLine();
                }
            }
            catch (BusinessException e)
            {
                Console.Write(e.Message);
                Console.ReadLine();
            }
        }
    }
}
