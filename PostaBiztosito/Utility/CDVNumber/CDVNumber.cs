﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostaBiztosito
{
    public static class CDVNumber 
    {
        /// <summary>
        /// Postabiztosito egyedi CDV számának a validátor fv.-e 
        /// </summary>
        /// <param name="cInputString"></param>
        /// <param name="nLength"></param>
        public static void IsPostaBiztositoCDV(string cInputString, int nLength)
        {
            int nOsszeg = 0;
            bool lHiba = false;

            // String hosszának öszevetése a megadott hosszúsággal
            if (!string.IsNullOrEmpty(cInputString) && cInputString.Length == nLength)
            {
                // Összeg kiszámolása a stringből
                for (int i = 0; i < nLength-1; i++)
                {
                    if (Int32.TryParse(cInputString.Substring(i, 1), out int cSzam))
                    {
                        if (i.isParos())
                        {
                            nOsszeg += cSzam;
                        }
                        else
                        {
                            nOsszeg += (cSzam * 2 >= 10 ? cSzam * 2 - 9 : cSzam * 2);
                        }
                    }
                    else
                    {
                        lHiba = true;
                        break;
                    }
                }

                if (lHiba)
                {
                    throw new Exception.BusinessException("Nem megfelelő az InputString.");
                }
                else
                {
                    string cMegadottCDV = cInputString.GetUtolsoKarakter();
                    int? nSzamoltCDV = nOsszeg.GetHelyiertek(1) == 0 ? 0 : (10 - nOsszeg.GetHelyiertek(1));

                    // Számolt és megadott CDV számok összehasonlítása
                    if (!nSzamoltCDV.ToString().Equals(cMegadottCDV))
                    {
                        throw new Exception.BusinessException("A megadott szám nem helyes.");
                    }
                }
            } 
            else
            {
                throw new Exception.BusinessException("A megadott szám hossza nem megfelelő.");
            }
        }
    }
}
