﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostaBiztosito
{
    public static class NumericExtension
    {
        /// <summary>
        /// Eldönti egy számról, hogy páros e (0 is páros szám) 
        /// </summary>
        /// <param name="nSzam"></param>
        /// <returns></returns>
        public static bool isParos(this int nSzam)
        {
            if (nSzam == 0 || nSzam % 2 == 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Visszaadja egy Szám paraméterben megadott helyiértékét
        /// </summary>
        /// <param name="nSzam"></param>
        /// <param name="nHelyiertek"></param>
        /// <returns></returns>
        public static int? GetHelyiertek(this int nSzam, int nHelyiertek)
        {
            if (nSzam != null)
            {
                string cSzam = nSzam.ToString();
                string cHelyiertek = nHelyiertek.ToString();

                if (cSzam.Length >= cHelyiertek.Length)
                {
                    return Convert.ToInt32(cSzam.Substring(cSzam.Length - cHelyiertek.Length, 1));
                }
            }

            return null;
        }
    }
}
