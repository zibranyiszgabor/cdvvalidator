﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostaBiztosito
{
    public static class StringExtension
    {
        /// <summary>
        /// Visszaadja egy string utolsó karakterét
        /// </summary>
        /// <param name="cInputString"></param>
        /// <returns></returns>
        public static string GetUtolsoKarakter(this string cInputString)
        {
            if (!string.IsNullOrEmpty(cInputString))
            {
               return cInputString.Substring(cInputString.Length - 1, 1);
            }

            return null;
        }
    }
}
