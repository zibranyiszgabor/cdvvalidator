﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PostaBiztosito;

namespace PostaBiztositoTeszt
{
    [TestClass]
    public class CDVNumberTest
    {
        [TestMethod]
        public void CDVNumberTests()
        {
            CDVNumber.IsPostaBiztositoCDV("958465122", 9);
            CDVNumber.IsPostaBiztositoCDV("111111118", 9);
            CDVNumber.IsPostaBiztositoCDV("661166116", 9);
            CDVNumber.IsPostaBiztositoCDV("1660", 4);
        }
    }
}
